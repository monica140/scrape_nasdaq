import requests
import pandas as pd
from bs4 import BeautifulSoup

"""Scrape market company listings and export their ISIN data.

Written to scrape tabula data from Nasdaq, Inc.
(http://www.nasdaqomxnordic.com/)
"""


def get_company_data(row, headers):
    """Get data from table row and return as a dictionary.

    Args:
        row (bs4.element.Tag): The tr tag object containing the data.

    Returns:
        dict: The company data.

    """
    row = str(row)
    soup = BeautifulSoup(row, 'html.parser')
    td = soup.find_all('td')

    data = {
        'name': td[headers['name']].get_text(),
        'symbol': td[headers['symbol']].get_text(),
        'isin': td[headers['isin']].get_text(),
    }

    return data


def get_headers(row):
    """Get column names and their index from table header.

    Args:
        row (bs4.element.Tag): The th tag object.

    Returns:
        dict: Table column names and their index.

    """
    row = str(row)
    soup = BeautifulSoup(row, 'html.parser')
    th = soup.find_all('th')
    headers = {}

    for i, h in enumerate(th):
        headers[h.get_text().lower()] = i

    return headers


def main():
    listings = {
        'STO': 'http://www.nasdaqomxnordic.com/shares/listed-companies/stockholm',
        'CPH': 'http://www.nasdaqomxnordic.com/shares/listed-companies/copenhagen',
        'HEL': 'http://www.nasdaqomxnordic.com/shares/listed-companies/helsinki',
        'OSL': 'http://www.nasdaqomxnordic.com/shares/listed-companies/norwegian-listed-shares',
        'ICE': 'http://www.nasdaqomxnordic.com/shares/listed-companies/iceland',
    }

    headers = {     # Needed as Nasdaq, Inc. blocks python agents
        'Accept': 'application/json, text/plain, */*',
        'Accept-Encoding': 'gzip, deflate',
        'User-Agent': 'Mozilla/5.0 XXXXX...'
    }

    for market, url in listings.items():
        r = requests.get(url, headers=headers, timeout=10)
        if r:
            text = r.text
            soup = BeautifulSoup(text, 'html.parser')
            rows = soup.find_all('tr')

            rows_list = []
            table_headers = {}

            for i, row in enumerate(rows):
                if i == 0:
                    table_headers = get_headers(row)
                    continue
                data = get_company_data(row, table_headers)
                data['market'] = market
                rows_list.append(data)

            df = pd.DataFrame(rows_list, columns=['name', 'symbol', 'market', 'isin'])

            try:
                filename = market + '.csv'
                df.to_csv('C:/Users/ST6/Documents/Scripts/Scrape ISIN Data/' + filename, index=False, header=True)
                print(market, 'csv exported')
            except:
                print('Error exporting', market, 'csv')


if __name__ == '__main__':
    main()
